import os
import subprocess

from mininet.link import TCLink
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.node import OVSController
from enum import Enum
from generate import generate_configuration

from common import fetch_net_hosts
from common import generate_bulk_tgen_model
from common import generate_every_x_tgen_model
from common import dump_link_configuration
from common import write_hosts
from common import write_hosts_list
from common import launch_tor_on_node
from common import create_node_dir
from common import wait_bootstrapped
from common import cleanup


class SetupTcpOverDTLS(Enum):
    NoDropping = 1
    DroppingShared = 2
    DroppingRemaining = 3


class ScenarioType(Enum):
    One = 1
    Two = 2
    Three = 3
    ReardonNoDropping = 4
    ReardonDroppingShared = 5
    ReardonDroppingRemaining = 6


def add_hosts(topo: Topo, prefix, count):
    hosts = []
    for i in range(count):
        host = topo.addHost('%s%d' % (prefix, i))
        hosts.append(host)
    return hosts


def add_hosts_and_link(topo: Topo, switch, prefix, count, **kwargs):
    hosts = add_hosts(topo, prefix, count)
    for host in hosts:
        topo.addLink(host, switch, **kwargs)
    return hosts


def scenario_tcp_over_dtls(scenario, data_dir, tor_version, setup: SetupTcpOverDTLS, tor_options, fix_delay=False, active_clients=8):
    assert scenario in [ScenarioType.ReardonNoDropping, ScenarioType.ReardonDroppingShared, ScenarioType.ReardonDroppingRemaining]

    os.environ['PATH'] = os.path.join('scripts') + os.pathsep + os.path.join('/vagrant', 'bin', tor_version, 'bin') + os.pathsep + os.environ['PATH']

    topo = Topo()

    switch_clients = topo.addSwitch('clients1')
    switch_guards  = topo.addSwitch('guards1')
    switch_middles = topo.addSwitch('middles1')
    switch_exits   = topo.addSwitch('exists1')
    switch_servers = topo.addSwitch('servers1')

    switches = [
        switch_clients,
        switch_guards ,
        switch_middles,
        switch_exits  ,
        switch_servers,
    ]

    clients     = add_hosts_and_link(topo , switch_clients , 'client' , 8)
    guards      = add_hosts_and_link(topo , switch_guards  , 'guard'  , 4)
    middles     = add_hosts_and_link(topo , switch_middles , 'middle' , 1)
    exits       = add_hosts_and_link(topo , switch_exits   , 'exit'   , 1)
    tgens       = add_hosts_and_link(topo , switch_servers , 'tgen'   , 1)
    authorities = add_hosts_and_link(topo , switch_servers , 'auth'   , 3)

    if not fix_delay:
        # NOTE: 50ms delay from client to exit 8.33 = 50 / (2 [Directions] * 3 [Links])
        if setup == SetupTcpOverDTLS.NoDropping:
            topo.addLink(switch_clients , switch_guards  , delay='8.33ms')
            topo.addLink(switch_guards  , switch_middles , delay='8.33ms')
            topo.addLink(switch_middles , switch_exits   , delay='8.33ms')
            topo.addLink(switch_exits   , switch_servers)
        elif setup == SetupTcpOverDTLS.DroppingRemaining:
            topo.addLink(switch_clients , switch_guards  , delay='8.33ms', loss=0.1)
            topo.addLink(switch_guards  , switch_middles , delay='8.33ms', loss=0.1)
            topo.addLink(switch_middles , switch_exits   , delay='8.33ms')
            topo.addLink(switch_exits   , switch_servers)
        elif setup == SetupTcpOverDTLS.DroppingShared:
            topo.addLink(switch_clients , switch_guards  , delay='8.33ms')
            topo.addLink(switch_guards  , switch_middles , delay='8.33ms')
            topo.addLink(switch_middles , switch_exits   , delay='8.33ms', loss=0.1)
            topo.addLink(switch_exits   , switch_servers)
    else:
        # NOTE: 50ms delay from client to exit 8.33 = 50 / (2 [Directions] * 3 [Links])
        if setup == SetupTcpOverDTLS.NoDropping:
            topo.addLink(switch_clients , switch_guards  , delay='25ms')
            topo.addLink(switch_guards  , switch_middles , delay='25ms')
            topo.addLink(switch_middles , switch_exits   , delay='25ms')
            topo.addLink(switch_exits   , switch_servers , delay='25ms')
        elif setup == SetupTcpOverDTLS.DroppingRemaining:
            topo.addLink(switch_clients , switch_guards  , delay='25ms', loss=0.1)
            topo.addLink(switch_guards  , switch_middles , delay='25ms', loss=0.1)
            topo.addLink(switch_middles , switch_exits   , delay='25ms')
            topo.addLink(switch_exits   , switch_servers , delay='25ms')
        elif setup == SetupTcpOverDTLS.DroppingShared:
            topo.addLink(switch_clients , switch_guards  , delay='25ms')
            topo.addLink(switch_guards  , switch_middles , delay='25ms')
            topo.addLink(switch_middles , switch_exits   , delay='25ms', loss=0.1)
            topo.addLink(switch_exits   , switch_servers , delay='25ms')

    net = Mininet(topo=topo, link=TCLink, autoPinCpus=True, controller=OVSController)

    fetch_net_hosts(
        net,
        switches,
        authorities,
        guards,
        middles,
        exits,
        clients,
        tgens
    )

    chutney_node_dir = generate_configuration(
        authorities=authorities,
        guards=guards,
        middles=middles,
        exits=exits,
        clients=clients,
        data_dir=data_dir
    )

    dump_link_configuration(net, chutney_node_dir)

    bulk_model = generate_bulk_tgen_model(
        data_dir=chutney_node_dir,
        peers=','.join(['%s:80' % tgen.IP() for tgen in tgens]),
        stream_name='stream_5M_bulk',
        stream_recvsize='5 MiB',
        stream_timeout='200 s',
    )

    try:
        net.start()

        tor_nodes = clients + guards + middles + exits + authorities

        write_hosts(net)
        write_hosts_list(net)

        for link in net.links:
            link.intf1.node.cmd("ethtool -K %s tso off" % link.intf1.name)
            link.intf1.node.cmd("ethtool -K %s gso off" % link.intf1.name)
            link.intf2.node.cmd("ethtool -K %s tso off" % link.intf2.name)
            link.intf2.node.cmd("ethtool -K %s gso off" % link.intf2.name)
            # link.intf1.node.cmd("sysctl -w net.ipv4.tcp_congestion_control=reno")
            # link.intf2.node.cmd("sysctl -w net.ipv4.tcp_congestion_control=reno")

        for node in tor_nodes:
            launch_tor_on_node(node, tor_options)

        for node in tgens:
            dir = create_node_dir(chutney_node_dir, node)
            node.cmd("tgen ./tgen-server.tgenrc.graphml > %s/log &" % (dir))

        wait_bootstrapped(tor_nodes)

        # input("Press ENTER to continue")

        runningCommands = []
        for idx, client in enumerate(clients):
            command = 'torpath -p %s %s %s -- tgen %s > %s/tgen-%s.log 2>&1' % (
                guards[int(idx / 2)].chutney_nick,
                middles[0].chutney_nick,
                exits[0].chutney_nick,
                bulk_model,
                client.chutney_dir,
                os.path.basename(bulk_model)
            )
            print(command)
            runningCommands.append(client.popen(command, shell=True))
            print('start client %s' % active_clients)
            active_clients -= 1
            if active_clients == 0:
                break

        for command in runningCommands:
            command.wait()
    finally:
        cleanup(net, data_dir)


def scenario(scenario: ScenarioType, data_dir, tor_version, tor_options):
    assert scenario in [ScenarioType.One, ScenarioType.Two, ScenarioType.Three]

    os.environ['PATH'] = os.path.join('scripts') + os.pathsep + os.path.join('/vagrant', 'bin', tor_version, 'bin') + os.pathsep + os.environ['PATH']

    topo = Topo()

    switch_clients = topo.addSwitch('clients1')
    switch_guards  = topo.addSwitch('guards1')
    switch_middles = topo.addSwitch('middles1')
    switch_exits   = topo.addSwitch('exists1')
    switch_servers = topo.addSwitch('servers1')

    switches = [
        switch_clients,
        switch_guards ,
        switch_middles,
        switch_exits  ,
        switch_servers,
    ]

    clients       = add_hosts_and_link(topo , switch_clients , 'client'       , 2)
    guards        = add_hosts_and_link(topo , switch_guards  , 'guard'        , 5)
    middles       = add_hosts_and_link(topo , switch_middles , 'middle'       , 5)
    exits         = add_hosts_and_link(topo , switch_exits   , 'exit'         , 5)
    authorities   = add_hosts_and_link(topo , switch_servers , 'auth'         , 3)

    iperf_clients = []
    iperf_servers = []
    if scenario in [ScenarioType.Two, ScenarioType.Three]:
        iperf_servers = add_hosts_and_link(topo , switch_middles , 'iperfs' , 4)
        iperf_clients = add_hosts_and_link(topo , switch_exits   , 'iperfc' , 4)

    if scenario == ScenarioType.One:
        topo.addLink(switch_clients , switch_guards  , delay='8.33ms')
        topo.addLink(switch_guards  , switch_middles , delay='8.33ms')
        topo.addLink(switch_middles , switch_exits   , delay='8.33ms', loss=0.1)
        topo.addLink(switch_exits   , switch_servers)
    elif scenario == ScenarioType.Two:
        topo.addLink(switch_clients , switch_guards  , bw=300)
        topo.addLink(switch_guards  , switch_middles , bw=300)
        topo.addLink(switch_middles , switch_exits   , bw=300)
        topo.addLink(switch_exits   , switch_servers)
    elif scenario == ScenarioType.Three:
        topo.addLink(switch_clients , switch_guards  , bw=2)
        topo.addLink(switch_guards  , switch_middles , bw=2)
        topo.addLink(switch_middles , switch_exits   , bw=2)
        topo.addLink(switch_exits   , switch_servers)

    tgens = add_hosts(topo, 'tgen', 4)
    topo.addLink(tgens[0], switch_servers)
    topo.addLink(tgens[1], switch_servers, bw=0.05*8) # 0.05 = 50 Kilobit per second; 0.05 * 8 = 50 Kilobyte per second = 400 Kilobit per second (very good audio quality)
    topo.addLink(tgens[2], switch_servers, bw=1.0*8)  # 1.00 = 1 Megabit per second;  1.00 * 8 = 1 Megabyte per second = 8000 Kilobit per second (HD with 30fps)
    topo.addLink(tgens[3], switch_servers)

    net = Mininet(topo=topo, link=TCLink, autoPinCpus=True, controller=OVSController)

    fetch_net_hosts(
        net,
        switches,
        authorities,
        guards,
        middles,
        exits,
        clients,
        tgens,
    )

    if scenario in [ScenarioType.Two, ScenarioType.Three]:
        fetch_net_hosts(
            net,
            iperf_clients,
            iperf_servers,
        )

    chutney_node_dir = generate_configuration(
        authorities=authorities,
        guards=guards,
        middles=middles,
        exits=exits,
        clients=clients,
        data_dir=data_dir
    )

    models = []

    models.append(generate_bulk_tgen_model(
        data_dir=chutney_node_dir,
        peers='%s:80' % tgens[0].IP(),
        stream_name='stream_5M_bulk',
        stream_recvsize='5 MiB',
        stream_timeout='200 s',
    ))
    models.append(generate_bulk_tgen_model(
        data_dir=chutney_node_dir,
        peers='%s:80' % tgens[1].IP(),
        stream_name='stream_50K_audio_bulk',
        stream_recvsize='50 KiB',
        stream_timeout='60 s',
    ))
    models.append(generate_bulk_tgen_model(
        data_dir=chutney_node_dir,
        peers='%s:80' % tgens[2].IP(),
        stream_name='stream_1M_video_bulk',
        stream_recvsize='1 MiB',
        stream_timeout='60 s',
    ))
    models.append(generate_every_x_tgen_model(
        data_dir=chutney_node_dir,
        peers='%s:80' % tgens[3].IP(),
        pause_time='10 s',
        stream_name='stream_2M_web',
        stream_recvsize='2 MiB',
        stream_timeout='60 s',
    ))
    bulk_model = models[0]

    dump_link_configuration(net, chutney_node_dir)

    try:
        net.start()

        tor_nodes = clients + guards + middles + exits + authorities

        write_hosts(net)
        write_hosts_list(net)

        for link in net.links:
            link.intf1.node.cmd("ethtool -K %s tso off" % link.intf1.name)
            link.intf1.node.cmd("ethtool -K %s gso off" % link.intf1.name)
            link.intf2.node.cmd("ethtool -K %s tso off" % link.intf2.name)
            link.intf2.node.cmd("ethtool -K %s gso off" % link.intf2.name)

        if tor_options:
            tor_options = subprocess.list2cmdline(tor_options)
        else:
            tor_options = ''

        for node in tor_nodes:
            launch_tor_on_node(node, tor_options)

        for node in tgens:
            node.cmd("tgen ./tgen-server.tgenrc.graphml > %s/log &" % create_node_dir(chutney_node_dir, node))

        if scenario in [ScenarioType.Two, ScenarioType.Three]:
            for node in iperf_servers:
                node.cmd("iperf3 -s > %s/iperfs.log 2>&1 &" % create_node_dir(chutney_node_dir, node))

            for idx, node in enumerate(iperf_clients):
                node.cmd("iperf3 -c %s -t 0 > %s/iperfc.log 2>&1 &" % (iperf_servers[idx].IP(), create_node_dir(chutney_node_dir, node)))

        wait_bootstrapped(tor_nodes)

        for model in models:
            print('Run %s...' % model)

            runningCommands = []

            command = 'torpath -p %s %s %s -- tgen %s > %s/tgen-%s-%s.log 2>&1' % (
                guards[0].chutney_nick,
                middles[0].chutney_nick,
                exits[0].chutney_nick,
                bulk_model,
                clients[0].chutney_dir,
                os.path.basename(bulk_model),
                os.path.basename(model)
            )
            print(command)
            runningCommands.append(clients[0].popen(command, shell=True))

            command = 'torpath -p %s %s %s -- tgen %s > %s/tgen-%s.log 2>&1' % (
                guards[1].chutney_nick,
                middles[0].chutney_nick,
                exits[0].chutney_nick,
                model,
                clients[1].chutney_dir,
                os.path.basename(model)
            )
            print(command)
            runningCommands.append(clients[1].popen(command, shell=True))

            for command in runningCommands:
                command.wait()

            print('(DONE)')
            print()
    finally:
        cleanup(net, data_dir)
