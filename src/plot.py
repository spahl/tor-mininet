import math
import sys
import os

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

from glob import glob

from argparse import Namespace
from matplotlib.backends.backend_pdf import PdfPages

import tornettools.plot

from tornettools.util import load_json_data
from tornettools.parse_tgen import __get_download_time
from tornettools.parse_tgen import __get_round_trip_time
from tornettools.plot import __plot_transfer_time
from tornettools.plot import __plot_cdf_figure
from tornettools.plot import __plot_round_trip_time

from scenarios import ScenarioType

plt.rc('font', family='cmr10')
plt.rc('font', size=30)
plt.rc('axes', axisbelow=True)
plt.rc('axes.formatter', use_mathtext=True)
plt.rc('figure', figsize=(16, 10))
plt.rc('legend', fontsize=25)


def plot_reardon(args, tor_versions):
    # NOTE: disable so we do not need `texlive`
    # matplotlib.rcParams['ps.useafm'] = True
    # matplotlib.rcParams['pdf.use14corefonts'] = True
    # matplotlib.rcParams['text.usetex'] = True

    os.makedirs('./plots', exist_ok=True)
    filename = './plots/paper-local-readorn.pdf'

    print(filename)

    with PdfPages(filename) as pages:
        scenarios = [
            ScenarioType.ReardonNoDropping,
            ScenarioType.ReardonDroppingRemaining,
            ScenarioType.ReardonDroppingShared,
        ]
        scenario_names = dict(
            ReardonNoDropping="ND",
            ReardonDroppingRemaining="DR",
            ReardonDroppingShared="SD",
        )
        for size in ['round_trip_time', '5242880']:
            file_sets = []
            labels = []
            for scenario in [scenario.name for scenario in scenarios]:
                for tor_version in tor_versions:
                    if tor_version == '3.5.18':
                        label = '%s: Vanilla' % scenario_names[scenario]
                    elif tor_version == '3.5.18-pctls':
                        label = '%s: PCTLS' % scenario_names[scenario]
                    else:
                        continue

                    files = list(glob(os.path.join(args.DATA_DIR, scenario, tor_version, 'nodes.*', "tgen.analysis.json.xz")))
                    label = '%s (n=%d)' % (label, len(files))

                    file_sets.append(files)
                    labels.append(label)

            if len(file_sets[0]) > 0:
                if size != 'round_trip_time':
                    # with SplitXAxisPlot(parts=1) as split:
                    with SplitXAxisPlot(parts=3, limits=[(0.0, 0.19), (2.1, 5.2), (9.0, 17.9)], gridspec_kw={'width_ratios': [1, 10, 3]}) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=['5242880'], plot_round_trip_time=False)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)
                else:
                    with SplitXAxisPlot(parts=1) as split:
                    # with SplitXAxisPlot(parts=1, limits=[(0.0, 0.139)]) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=[], plot_round_trip_time=True)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)


def plot_other(args, tor_versions):
    scenarios = [
        ScenarioType.One,
        ScenarioType.Two,
        ScenarioType.Three,
    ]

    os.makedirs('./plots', exist_ok=True)
    for size in ['round_trip_time', '51200','1048576','2097152','5242880']:
        filename = './plots/paper-local-%s.pdf' % size

        print(filename)

        with PdfPages(filename) as pages:
            file_sets = []
            labels = []
            margin = 0.20
            for scenario in [scenario.name for scenario in scenarios]:
                if not os.path.exists(os.path.join(args.DATA_DIR, scenario)):
                    continue
                for tor_version in tor_versions:
                    # NOTE: translation for paper
                    if scenario == 'Two':
                        label_text = 'Two'
                    else:
                        label_text = scenario

                    if tor_version == '3.5.18':
                        label = '%s: Vanilla' % label_text
                    elif tor_version == '3.5.18-pctls':
                        label = '%s: PCTLS' % label_text
                    else:
                        continue

                    files = list(glob(os.path.join(args.DATA_DIR, scenario, tor_version, 'nodes.*', "tgen.analysis.json.xz")))
                    label = '%s (n=%d)' % (label, len(files))

                    file_sets.append(files)
                    labels.append(label)

            if len(file_sets) > 0 and len(file_sets[0]) > 0:
                if size == '51200':
                    with SplitXAxisPlot(parts=2, limits=[(0.0, 0.19), (0.9, 2.19)], gridspec_kw={'width_ratios': [1, 13]}) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=[size], plot_round_trip_time=False)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)
                elif size == '1048576':
                    with SplitXAxisPlot(parts=3, limits=[(0.0, 0.19),(0.9, 1.3),(20.0,40.0)], gridspec_kw={'width_ratios': [1, 10, 3]}) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=[size], plot_round_trip_time=False)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)
                elif size == '2097152':
                    with SplitXAxisPlot(parts=1, limits=[]) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=[size], plot_round_trip_time=False)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)
                elif size == '5242880':
                    with SplitXAxisPlot(parts=2, limits=[(0.0,6.0),(90.0,350.0)]) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=[size], plot_round_trip_time=False)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)
                elif size == 'round_trip_time':
                    with SplitXAxisPlot(parts=2, limits=[(0.0, 0.29),(0.61, 1.19)], gridspec_kw={'width_ratios': [10, 3]}) as split:
                        legend = plot(pages, split.axs, labels, file_sets, plot_sizes=[], plot_round_trip_time=True)
                        split.set_legend(*legend)
                    pages.savefig(split.figure)
                else:
                    assert False


legend_lines = None
legend_labels = None


def plot_finish(args, lines, labels, filename):
    global legend_lines
    global legend_labels

    legend_lines = lines
    legend_labels = labels

    plt.gca().set_yticklabels(['','','0.3','0.4','0.5','0.6','0.7','0.8','','','0.93','0.94','0.95','0.96','0.97','0.98'], minor=True)
    plt.tick_params(axis='both', which='major', labelsize=22)
    plt.tick_params(axis='both', which='minor', labelsize=17)
    plt.grid(True, axis='both', which='minor', color='0.1', linestyle=':', linewidth='0.5')
    plt.grid(True, axis='both', which='major', color='0.1', linestyle=':', linewidth='1.0')


def plot_transfer_time(args, torperf_dbs, tornet_dbs, bytes_key, xlabel):
    for tornet_db in tornet_dbs:
        tornet_db['data'] = [tornet_db['dataset'][i][bytes_key] for i, _ in enumerate(tornet_db['dataset']) if bytes_key in tornet_db['dataset'][i]]
    for torperf_db in torperf_dbs:
        torperf_db['data'] = [torperf_db['dataset']['download_times'][bytes_key]]

    dbs_to_plot = torperf_dbs + tornet_dbs

    __plot_cdf_figure(args, dbs_to_plot, f"transfer_time_{bytes_key}",
        yscale="taillog",
        xlabel=xlabel
    )


tornettools.plot.__plot_finish = plot_finish
tornettools.plot.__plot_transfer_time = plot_transfer_time


def plot(pdf_pages: PdfPages, axs, labels, file_sets, plot_round_trip_time=True, plot_sizes=['51200','1048576','2097152','5242880','10485760']):
    pages = pdf_pages

    size_to_xlabel = {
        '51200': 'Transfer Time (s): 50 kB/s (audio)',
        '1048576': 'Transfer Time (s): 1 MB/s (video)',
        '2097152': 'Transfer Time (s): 2 MB every 10s (web)',
        '5242880': 'Transfer Time (s): 5 MB (bulk)',
        '10485760': 'Transfer Time (s): 10 MB (bulk)',
    }

    transfer_tornet_dbs = []
    round_trip_time_tornet_dbs = []
    for label, files in zip(labels, file_sets):
        transfer_dataset = []
        round_trip_time_dataset = []
        for file in files:
            data_raw = load_json_data(file)
            data_raw_keys = list(data_raw['data'].keys())
            assert len(data_raw_keys) == 1
            data = dict(
                data=dict(
                    perfclient=data_raw['data'][data_raw_keys[0]]
                )
            )

            download_time = __get_download_time(data, -1, -1, 'time_to_last_byte_recv')
            transfer_dataset.append(download_time)

            round_trip_time_dataset.append(
                __get_round_trip_time(data, -1, -1)
            )
        transfer_tornet_dbs.append({
            'dataset': transfer_dataset,
            'label': label,
            'color': None
        })
        round_trip_time_tornet_dbs.append({
            'dataset': round_trip_time_dataset,
            'label': label,
            'color': None
        })

    args = Namespace(plot_pngs=False)

    if plot_round_trip_time:
        for ax in axs:
            plt.axes(ax)
            for db in round_trip_time_tornet_dbs:
                data = []
                for ds in db['dataset']:
                    data += ds
                print("rtt;%s;%s;%s" % (db['label'], np.mean(data), np.std(data)))
            print()
            __plot_round_trip_time(args, [], round_trip_time_tornet_dbs)

    if plot_sizes:
        for size in plot_sizes:
            for ax in axs:
                plt.axes(ax)
                for db in transfer_tornet_dbs:
                    data = []
                    for ds in db['dataset']:
                        data += ds['ALL']
                    print(f"transfer {size};%s;%s;%s" % (db['label'], np.mean(data), np.std(data)))
                print()
                # NOTE: For 2M ignore "Three" measuements, because there are only very few!
                filtered_transfer_tornet_dbs = []
                for db in transfer_tornet_dbs:
                    if 'Three' in db['label'] and size == '2097152':
                        continue
                    filtered_transfer_tornet_dbs.append(db)
                plot_transfer_time(args, [], filtered_transfer_tornet_dbs, size, size_to_xlabel[size])

    return legend_lines, legend_labels


class SplitXAxisPlot:
    def __init__(self, parts=1, limits=[], **kwargs):
        figure, axs = plt.subplots(1, parts, sharey=True, **kwargs)

        if parts == 1:
            axs = [axs]

        self.figure = figure
        self.default_figure = plt.figure
        self.default_xlim = plt.xlim
        self.lines = None
        self.labels = None
        self.axs = axs

        plt.figure = lambda *args: figure
        plt.xlim = lambda *args,**kwargs: None

        for idx, (left, right) in enumerate(limits):
            self.axs[idx].set_xlim(left, right)

    def set_legend(self, lines, labels):
        self.lines = lines
        self.labels = labels

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        plt.tight_layout(pad=0.3)

        total_ax = self.figure.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        plt.grid(False)
        box = total_ax.get_position()
        total_ax.set_xlim(0, 1.0)
        total_ax.set_ylim(0, 1.0)

        total_ax.set_xlabel(self.axs[0].get_xlabel())
        self.axs[0].set_xlabel(None)

        for ax in self.axs[1:]:
            ax.set_xlabel(None)
            ax.set_ylabel(None)

        for ax in self.axs:
            percent = 0.3
            box = ax.get_position()
            ax.set_position([box.x0, box.y0 + box.height * percent, box.width, box.height * (1.0 - percent)])

        if len(self.labels) <= 2:
            ncol = 1
        elif len(self.labels) <= 4:
            ncol = 2
        elif len(self.labels) <= 6:
            ncol = 3
        else:
            assert False

        total_ax.legend(self.lines, self.labels, loc='lower center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=ncol)

        x, y = total_ax.xaxis.label.get_position()
        total_ax.xaxis.set_label_coords(x, y + 0.225)

        if len(self.axs) >= 2:
            kwargs = dict(color='k', clip_on=False)
            for idx in range(0, len(self.axs)-1):
                self.axs[idx].spines['right'].set_visible(False)
                self.axs[idx+1].spines['left'].set_visible(False)
                self.axs[idx+1].tick_params(axis='y', which='both', left=False)

                dx, dy = total_ax.transAxes.inverted().transform((10.0, 10.0))
                dx, dy = math.fabs(dx) / 10.0, math.fabs(dy) / 10.0
                ax = self.axs[idx]
                transform = ax.transAxes + total_ax.transData.inverted()
                x, y = transform.transform((1.0, 1.0))
                total_ax.plot((x - dx, x + dx), (y - dy, y + dy), lw=1.0, color='k', clip_on=False)
                x, y = transform.transform((1.0, 0.0))
                total_ax.plot((x - dx, x + dx), (y - dy, y + dy), lw=1.0, color='k', clip_on=False)

                ax = self.axs[idx+1]
                transform = ax.transAxes + total_ax.transData.inverted()
                x, y = transform.transform((0.0, 1.0))
                total_ax.plot((x - dx, x + dx), (y - dy, y + dy), lw=1.0, color='k', clip_on=False)
                x, y = transform.transform((0.0, 0.0))
                total_ax.plot((x - dx, x + dx), (y - dy, y + dy), lw=1.0, color='k', clip_on=False)

        plt.figure = self.default_figure
        plt.xlim = self.default_xlim
        plt.close()
