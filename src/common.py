import os
import time
import subprocess

from logging import WARNING, INFO
from collections import defaultdict

import networkx
from mininet.log import lg

def generate_every_x_tgen_model(data_dir, peers, pause_time, stream_name, stream_recvsize, stream_timeout, socksproxy='127.0.0.1:9050'):
    graph = networkx.DiGraph()
    graph.add_node(
        'start',
        loglevel='info',
        socksproxy=socksproxy,
        peers=peers,
        packetmodelmode='path',
    )
    graph.add_node(
        'pause',
        time=pause_time,
    )
    graph.add_node(
        stream_name,
        sendsize='1000 bytes',
        recvsize=stream_recvsize,
        stallout='0 s',
        timeout=stream_timeout,
    )
    graph.add_node(
        'end',
        time='5 minutes'
    )
    graph.add_edge('start', 'pause')
    graph.add_edge('pause', 'pause')
    graph.add_edge('pause', stream_name)
    graph.add_edge(stream_name, 'end')
    dir = os.path.join(data_dir, 'conf')
    os.makedirs(dir, exist_ok=True)
    filename = os.path.join(dir, 'tgen-%s.tgenrc.graphml' % stream_name)
    networkx.write_graphml(graph, filename)
    return filename


def generate_bulk_tgen_model(data_dir, peers, stream_name, stream_recvsize, stream_timeout, socksproxy='127.0.0.1:9050'):
    graph = networkx.DiGraph()
    graph.add_node(
        'start',
        loglevel='info',
        socksproxy=socksproxy,
        peers=peers,
        packetmodelmode='path',
    )
    graph.add_node(
        stream_name,
        sendsize='1000 bytes',
        recvsize=stream_recvsize,
    )
    graph.add_node(
        'end',
        time='5 minutes'
    )
    graph.add_edge('start', stream_name)
    graph.add_edge(stream_name, stream_name)
    graph.add_edge(stream_name, 'end')
    dir = os.path.join(data_dir, 'conf')
    os.makedirs(dir, exist_ok=True)
    filename = os.path.join(dir, 'tgen-%s.tgenrc.graphml' % stream_name)
    networkx.write_graphml(graph, filename)
    return filename


def cmd(command, level=INFO):
    subprocess.run(['bash', '-c', command], check=True, capture_output=lg.level > level)


def wait_bootstrapped(tor_nodes):
    print("Tor log first three lines...")
    while True:
        all_exists = True
        for node in tor_nodes:
            filename = '%s/info.log' % node.chutney_dir
            all_exists = all_exists and os.path.exists(filename) and os.path.getsize(filename) > 100
        if all_exists:
            break
        time.sleep(1)
    for node in tor_nodes:
        print(node.name, node.chutney_nick)
        subprocess.run(['head', '-n', '3', '%s/info.log' % node.chutney_dir])

    print("Wait until bootstrapped...")
    bootstrapped = defaultdict(bool)
    while True:
        all_bootstrapped = True
        for node in tor_nodes:
            process = subprocess.run(['grep', 'Bootstrapped 100', '%s/info.log'% node.chutney_dir], capture_output=True)
            all_bootstrapped &= process.returncode == 0
            if process.returncode == 0:
                if not bootstrapped[node.chutney_nick]:
                    print('Bootstrapped %s (%s)' % (node.chutney_nick, node.chutney_dir))
                    bootstrapped[node.chutney_nick] = True
        if all_bootstrapped:
            break
        time.sleep(1)


def dump_link_configuration(net, chutney_node_dir):
    with open('%s/topo.log' % chutney_node_dir, 'w') as f:
        for link in net.links:
            assert link.intf1.params == link.intf2.params
            if link.intf1.params:
                f.write('%-15s %-15s -> %-15s %-15s' % (link.intf1.node, link.intf1, link.intf2.node, link.intf2))
                for key, value in link.intf1.params.items():
                    f.write('%s=%s ' % (key, value))
                f.write('\n')
            else:
                f.write('%-15s %-15s -> %-15s %-15s\n' % (link.intf1.node, link.intf1, link.intf2.node, link.intf2))
    cmd('cat %s/topo.log' % chutney_node_dir, WARNING)


def fetch_net_hosts(net, *host_lists):
    # NOTE: replaces the hostname with its respective Host object
    for hosts in host_lists:
        for idx, host in enumerate(hosts):
            hosts[idx] = net.get(host)


def cleanup(net, data_dir):
    cmd('killall -SIGTERM -w tor || true')
    cmd('killall -SIGTERM -w iperf3 || true')
    cmd('killall -SIGTERM -w tgen || true')
    net.stop()

    uid = os.environ.get('SUDO_UID', 0)
    gid = os.environ.get('SUDO_GID', 0)
    uid, gid = 1000, 1000
    subprocess.run(['chown', '-R', '%s:%s' % (uid, gid), data_dir])


def launch_tor_on_node(node, tor_options):
    if tor_options:
        tor_options = subprocess.list2cmdline(tor_options)
    else:
        tor_options = ''

    node.cmd('tor -f %s/torrc --ControlSocket 0 --AssumeReachable 1 --RunAsDaemon 0 --DirPort 7000 --ORPort 5000 --SocksPort 9050 --ControlPort 9051 %s > %s/startup.log 2>&1 &' % (node.chutney_dir, tor_options, node.chutney_dir))


def write_hosts(net):
    with open('/etc/hosts', 'w') as f:
        f.write("# Static table lookup for hostnames.\n")
        f.write("# See hosts(5) for details.\n")
        f.write("127.0.0.1       localhost\n")
        for node in net.hosts:
            f.write("%s       %s\n" % (node.IP(), node.name))


def write_hosts_list(net):
    # TODO: Is there are better way to get all hosts in mininet?
    with open('/tmp/mininet_hosts', 'w') as f:
        for node in net.hosts:
            f.write("%s\n" % node.name)


def create_node_dir(chutney_node_dir, node):
    dir = os.path.join(chutney_node_dir, node.name)
    os.makedirs(dir)
    return dir
