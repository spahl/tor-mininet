import sys
import os

def node(ip, **kwargs):
    parent = Node()
    parent._env['ip'] = ip
    node = Node(parent=parent, **kwargs)
    return node


def authority(ip):
    return node(ip, tag="auth", authority=1, relay=1, torrc="authority.tmpl")


def guard(ip):
    return node(ip, tag="guard", relay=1, torrc="relay-non-exit.tmpl")


def middle(ip):
    return node(ip, tag="middle", relay=1, torrc="relay-non-exit.tmpl")


def exit(ip):
    return node(ip, tag="exit", relay=1, exit=1, torrc="relay.tmpl")


def client(ip):
    return node(ip, tag="client", client=1, torrc="client.tmpl")


def generate_configuration(authorities=[], guards=[], middles=[], exits=[], clients=[], chutney_path='vendor/chutney', data_dir='net'):
    global Node

    chutney_path = os.path.join(os.getcwd(), chutney_path)
    chutney_data_dir = os.path.join(os.getcwd(), data_dir)

    sys.path.append(os.path.join(chutney_path, 'lib'))
    os.environ['CHUTNEY_PATH'] = chutney_path
    os.environ['CHUTNEY_DATA_DIR'] = chutney_data_dir

    # NOTE: always delete the nodes syslink otherwise it is maybe stale!
    try:
        os.remove(os.path.join(chutney_data_dir, 'nodes'))
    except FileNotFoundError:
        pass

    import chutney

    from chutney import TorNet
    from chutney.TorNet import TorEnviron
    from chutney.TorNet import Network
    from chutney.TorNet import Node
    from chutney.TorNet import ConfigureNodes
    from chutney.TorNet import DEFAULTS

    TorNet._BASE_ENVIRON = TorEnviron(chutney.Templating.Environ(**DEFAULTS))
    TorNet._THE_NETWORK = network = Network(TorNet._BASE_ENVIRON)

    nodes = []
    for host in authorities:
        node = authority(host.IP())
        node.__dict__['mininet'] = host
        nodes.append(node)
    for host in guards:
        node = guard(host.IP())
        node.__dict__['mininet'] = host
        nodes.append(node)
    for host in middles:
        node = middle(host.IP())
        node.__dict__['mininet'] = host
        nodes.append(node)
    for host in exits:
        node = exit(host.IP())
        node.__dict__['mininet'] = host
        nodes.append(node)
    for host in clients:
        node = client(host.IP())
        node.__dict__['mininet'] = host
        nodes.append(node)

    ConfigureNodes(nodes)

    for node in nodes:
        node.mininet.__dict__['chutney'] = node
        node.mininet.__dict__['chutney_nick'] = node._env._get_nick(node._env)
        node.mininet.__dict__['chutney_dir'] = node._env._get_dir(node._env)

    network.configure()

    return network.dir
