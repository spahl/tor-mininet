#!/usr/bin/env python3
import os
import subprocess
import sys
import argparse
import glob
import mininet.log

from common import cmd
from scenarios import scenario
from scenarios import scenario_tcp_over_dtls
from scenarios import ScenarioType
from scenarios import SetupTcpOverDTLS


def parse_and_plot(args, scenarios, tor_versions):
    sys.path.append(os.path.join('vendor/tornettools/tornettools'))

    from plot import plot_reardon
    from plot import plot_other

    for scenario in scenarios:
        for tor_version in tor_versions:
            dir = os.path.join(args.DATA_DIR, scenario, tor_version, '*')
            for node_dir in glob.glob(dir):
                if os.path.basename(node_dir) == 'nodes':
                    continue
                result = os.path.join(node_dir, "tgen.analysis.json.xz")
                if os.path.exists(result):
                    continue
                if not os.path.exists(node_dir):
                    continue
                name = '%s-%s' % (scenario, tor_version)
                patterns = [
                    "tgen-tgen-stream_50K_audio_bulk.tgenrc.graphml.log",
                    "tgen-tgen-stream_1M_video_bulk.tgenrc.graphml.log",
                    "tgen-tgen-stream_2M_web.tgenrc.graphml.log",
                    "tgen-tgen-stream_5M_bulk.tgenrc.graphml.log",
                    "tgen-tgen-stream_1M_video.tgenrc.graphml.log",
                    "tgen-tgen-stream_10M_bulk.tgenrc.graphml.log",
                    "tgen-tgen-stream_50K_audio.tgenrc.graphml.log",
                ]
                patterns = "-e '" + "' -e '".join(patterns) + "'"
                command = f"tgentools parse -n '{name}' -m 16 {patterns} --complete ."
                print(command)
                cwd = os.getcwd()
                try:
                    os.chdir(node_dir)
                    subprocess.run(command, shell=True, check=True)
                except PermissionError:
                    pass
                finally:
                    os.chdir(cwd)

    plot_reardon(args, tor_versions)
    plot_other(args, tor_versions)


def main():
    scenarios = [choice.name for choice in ScenarioType]
    tor_versions = list(os.listdir('bin'))

    parser = argparse.ArgumentParser()
    parser.add_argument('DATA_DIR', help='directory to use for the experiment')
    parser.add_argument('-s', '--scenario',  choices=scenarios, help='chose a scenario to run')
    parser.add_argument('-t', '--tor-version', choices=tor_versions, help='select the tor version')
    parser.add_argument('-p', '--parse-and-plot', action='store_true', help='parse output and plot scenarios')
    parser.add_argument('-m', '--term', action='store_true', help='launch iftop and nyx terimnals')
    parser.add_argument('-a', '--active-clients', nargs=1, dest='active_clients', help='Number of active clients in Reardon scenario.', default=['8'])
    parser.add_argument('--tor-options', nargs=argparse.REMAINDER, help='launch tor nodes with this additional options')
    args = parser.parse_args()

    if args.parse_and_plot:
        parse_and_plot(args, scenarios, tor_versions)
        return

    if os.geteuid() != 0:
        os.execvp("sudo", ["sudo", "-E"] + sys.argv)

    if args.term:
        terminal = os.environ.get('TERM', 'false')
        with open('/tmp/mininet_hosts') as f:
            hosts = f.read().split('\n')
        hosts = ['client0', 'client1', 'guard0', 'guard1', 'middle0', 'exit0']
        for host in hosts:
            subprocess.run(['bash', '-c', f"{terminal} -t '{host} - iftop' -e m {host} iftop -PN & sleep 0.1"])
        for host in hosts:
            subprocess.run(['bash', '-c', f"{terminal} -t '{host} - nyx' -e m {host} nyx & sleep 0.1"])
        return

    if args.tor_options:
        tor_options_path = subprocess.list2cmdline(args.tor_options)
        tor_options_path = tor_options_path.replace(' ', '-')
        data_dir = os.path.join(args.DATA_DIR, args.scenario, args.tor_version, tor_options_path)
    else:
        data_dir = os.path.join(args.DATA_DIR, args.scenario, args.tor_version)

    os.makedirs(data_dir, exist_ok=True)

    mininet.log.setLogLevel('warn')

    cmd('killall -SIGKILL -w tor || true')
    cmd('killall -SIGKILL -w iperf3 || true')
    cmd('killall -SIGKILL -w tgen || true')
    cmd('killall -SIGKILL -w torpath || true')
    cmd('mn -c 2> /dev/null')

    type = ScenarioType[args.scenario]

    if type == ScenarioType.One:
        scenario(ScenarioType.One, data_dir, args.tor_version, args.tor_options)
    elif type == ScenarioType.Two:
        scenario(ScenarioType.Two, data_dir, args.tor_version, args.tor_options)
    elif type == ScenarioType.Three:
        scenario(ScenarioType.Three, data_dir, args.tor_version, args.tor_options)
    elif type == ScenarioType.ReardonNoDropping:
        scenario_tcp_over_dtls(type, data_dir, args.tor_version, SetupTcpOverDTLS.NoDropping, args.tor_options, fix_delay=False, active_clients=int(args.active_clients[0]))
    elif type == ScenarioType.ReardonDroppingShared:
        scenario_tcp_over_dtls(type, data_dir, args.tor_version, SetupTcpOverDTLS.DroppingShared, args.tor_options, fix_delay=False, active_clients=int(args.active_clients[0]))
    elif type == ScenarioType.ReardonDroppingRemaining:
        scenario_tcp_over_dtls(type, data_dir, args.tor_version, SetupTcpOverDTLS.DroppingRemaining, args.tor_options, fix_delay=False, active_clients=int(args.active_clients[0]))


if __name__ == '__main__':
    main()
