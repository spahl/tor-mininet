Vagrant.configure("2") do |config|
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
    v.cpus = 4
  end
  config.vm.box = "ubuntu/kinetic64"
  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    set -ex

    sudo DEBIAN_FRONTEND=noninteractive apt update
    sudo DEBIAN_FRONTEND=noninteractive apt install -yq build-essential openvswitch-switch openvswitch-testcontroller python3-pip python3-matplotlib python3-numpy python3-networkx python3-stem libigraph-dev libglib2.0-dev libssl-dev libevent-dev autoconf iperf3 cmake git mininet pkg-config

    cd /vagrant

	mkdir -p bin
	mkdir -p vendor

    cd vendor
	git clone https://gitlab.com/spahl/tor || true
	git clone https://git.torproject.org/chutney.git || true
	git clone https://github.com/shadow/tgen.git || true
	git clone https://github.com/shadow/tornettools.git || true
    cd /vagrant

	mkdir -p bin/3.5.18
	mkdir -p bin/3.5.18-pctls

    cd vendor/chutney
    git checkout c825cba0bcd813c644c6ac069deeb7347d3200ee
    git apply ../chutney.patch || true
    cd /vagrant

    cd vendor/tgen
    git checkout 77a0d12eb09b89816b8b8141e930a7c759cd3cac
    git apply ../tgen.patch || true
	mkdir -p build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=`realpath ../../../bin/3.5.18`
    make install
	cmake .. -DCMAKE_INSTALL_PREFIX=`realpath ../../../bin/3.5.18-pctls`
    make install
    cd /vagrant

    cd vendor/tgen/tools
    sudo pip install .
    cd /vagrant

    cd vendor/tornettools
    git checkout v1.1.0
    sudo pip install .
    cd /vagrant

    cd vendor/tor
    git checkout vanilla
    cat configure.ac | grep -v AC_PROG_CC_C99 > configure.ac.new
    mv configure.ac.new configure.ac
    ./autogen.sh
    ./configure --prefix=`realpath ../../bin/3.5.18` --disable-seccomp --disable-asciidoc --disable-unittests
    make -j 16
    make install
    make clean
    cd /vagrant

    cd vendor/tor
    git checkout pctls
    cat configure.ac | grep -v AC_PROG_CC_C99 > configure.ac.new
    mv configure.ac.new configure.ac
    ./autogen.sh
    ./configure --prefix=`realpath ../../bin/3.5.18-pctls` --disable-seccomp --disable-asciidoc --disable-unittests
    make -j 16
    make install
    make clean
    cd /vagrant

    sudo systemctl start ovs-vswitchd.service
    sudo killall ovs-testcontroller
  SHELL
end
