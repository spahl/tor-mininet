#!/usr/bin/env python3
import sys
import stem
import stem.control
import subprocess
import argparse

from stem.control import Controller
from glob import glob


def get_all_relay_fingerprints():
    relays = {}

    with Controller.from_port() as controller:
      controller.authenticate()

      for desc in controller.get_network_statuses():
          relays[desc.nickname] = desc.fingerprint

    return relays


# Source: https://stem.torproject.org/tutorials/to_russia_with_love.html#custom-path-selection
# Altered to match our use case.
def run_command_over_circuit(command, path):
    with stem.control.Controller.from_port() as controller:
        controller.authenticate()

        circuit_id = controller.new_circuit(path, await_build = True)

        def attach_stream(stream):
            if stream.status == 'NEW':
                print('Attach stream %s (%s) to circuit %s\n' % (stream.id, stream.status, circuit_id))
                controller.attach_stream(stream.id, circuit_id)

        controller.add_event_listener(attach_stream, stem.control.EventType.STREAM)

        try:
            controller.set_conf('__LeaveStreamsUnattached', '1')
            process = subprocess.run(command)
        finally:
            controller.remove_event_listener(attach_stream)
            controller.set_conf('__LeaveStreamsUnattached', '0')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='path', description='create a fixed path/circuit through the Tor network', allow_abbrev=True)

    parser.add_argument('-p', '--path', nargs='+', help='path through the network, pick one or more hops', default=['test001relay', 'test003relay', 'test004exit'])
    parser.add_argument('-l', '--list', action='store_true', help='list all currently available hops. You have to launch the network at least once, otherwise the list is empty!')
    parser.add_argument('COMMAND', nargs=argparse.REMAINDER)

    args = parser.parse_args()

    try:
        relays = get_all_relay_fingerprints()

        if args.list:
            for relay in relays:
                print('- %s (%s)' % (relay, relays[relay]))
        elif args.COMMAND:
            if args.path:
                bailout = False
                for path in args.path:
                    if path not in relays:
                        print("%s isn't a valid hop name!" % path)
                        bailout = True
            if bailout:
                sys.exit(0)
            command = args.COMMAND
            if '--' == command[0]:
                command = command[1:]
            run_command_over_circuit(
                command=command,
                path=args.path
            )
    except KeyboardInterrupt:
        pass
