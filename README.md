# Tor Mininet

A local Tor testbed system based on [mininet](https://github.com/mininet/mininet) that is similar to
[Chutney](https://github.com/torproject/chutney); in fact, we use Chutney, but
only for generating configuration for the nodes; but allows more fine control
over the topology, bandwidth and packet loss.

It only supports specific scenarios at the moment that we needed for our
[paper](https://github.com/spahl/hydra-popets2023).

## Dependencies

* git
* vagrant
* virtualbox

## Setup

Mininet does not work with docker, we need a virtual machine to run it.
This is why we use `vagrant`.

```
$ sudo pacman --needed -S git vagrant # Arch Linux based
$ git clone https://github.com/spahl/tor-mininet
$ cd tor-mininet
$ vagrant up
$ vagrant ssh
$ cd /vagrant
```

After starting and logging into the virtual machine (VM), the current directory is mounted under `/vagrant`.

## Usage

We control how circuits are build with the `scripts/torpath` script.
The data flow is always from "guards", "middles" to "exits".

*Beware*: In the examples below all log files are written to `/tmp/experiments`.
This directory is inside of the VM.
If you want to keep this data make it relative to `/vagrant`, e.g. `$ ./run ./experiments` or copy it.
All commands below must be run inside of the VM.

To run one experiment for each scenario:

```sh
$ ./run /tmp/experiments
```

One scenario with a specific Tor binary is executed per line. Each line will take some time:

* Lines with scenario One, Two and Three: 4 models are measured, each will take 5 minutes $\Rightarrow$ 20 minutes per line
* Other lines will take 5 minutes
* In **total** one experiment will take 2 hours and 30 minutes

To run only one scenario (this will take around 20 minutes):

```
$ ./src/main.py -s One -t 3.5.18 /tmp/experiments
```

To plot the result and output into `plots/` as PDF:

```sh
$ ./src/main.py -p /tmp/experiments
```

*Note*: The `plots/` directory is in the current directory on the host machine.
You can easily access and view plots in this directory.
The output should be similar to Figure 13 and 14 in our paper.
You should run at least two experiments before plotting.

It is possible to set Tor configuration options, for every node, via the command line.
It must be the last option!

```sh
$ ./src/main.py -s One -t 3.5.18 /tmp/experiments --tor-options --Schedulers Vanilla
```

The following command only works if run you an experiment on your host machine or have a VM with GUI.
To observe what is going on after starting an experiment, you can launch `iftop` and `nyx` terminals for every Tor node:

```sh
$ ./src/main.py -m
```
